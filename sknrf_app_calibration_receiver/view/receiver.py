import os
from itertools import combinations

import numpy as np
from PySide2.QtGui import QIcon

from sknrf.device.signal import tf
from sknrf.device.instrument import rfreceiver
from sknrf.device.instrument import rfsource
from sknrf.device.instrument.rfreceiver import NoRFReceiver
from sknrf.device.instrument.rfsource import NoRFSource
from sknrf.model.calibration.base import calkit_connector_map
from sknrf_app_calibration_receiver.model.receiver import AmplitudeModel, PhaseModel
from sknrf.enums.device import Instrument
from sknrf.view.desktop.calibration.wizard.base import AbstractCalibrationWizard
from sknrf.view.desktop.calibration.wizard.base import AbstractContentPage, AbstractConclusionPage
from sknrf.view.desktop.calibration.wizard.base import AbstractPortPage, AbstractInstrumentPage, AbstractRequirementsPage
from sknrf.utilities.patterns import export


class AmplitudePortPage(AbstractPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class AmplitudeInstrumentPage(AbstractInstrumentPage):

    def __init__(self, _parent):
        super(AmplitudeInstrumentPage, self).__init__(_parent)
        self.add_instrument(Instrument.RFRECEIVER)


class AmplitudeRequirementsPage(AbstractRequirementsPage):

    def __init__(self, _parent):
        super(AmplitudeRequirementsPage, self).__init__(_parent)
        self.add_requirement("LF Source is OFF for all ports.")
        self.add_requirement("RF Source is NOT connected for calibration port.")
        self.add_requirement("RF Source is connected for measurement port.")
        self.add_requirement("RF Source is ON for measurement port.")
        self.add_requirement("RF Source.a_p > min for measurement port.")
        self.add_requirement("RF Receiver is connected for all ports.")
        self.add_requirement("RF Receiver is ON for calibration port.")

        self.add_recommendation("RF ZTuner.z_set == 50 Ohm for all ports.")
        self.connect_signals()

    def check_requirements(self):
        super(AmplitudeRequirementsPage, self).check_requirements()
        state = [True]*len(self.requirementsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                state[1] = state[1] and type(port.rfsource) is NoRFSource
                state[6] = state[6] and port.rfreceiver.on
            else: # Measurement Port
                state[2] = state[2] and type(port.rfsource) is not NoRFSource
                state[3] = state[3] and port.rfsource.on
                info = port.rfsource.info
                min, abs_tol = info["a_p"].min, info["a_p"].abs_tol
                state[4] = state[4] and np.all(np.abs(tf.pk(port.rfsource.a_p)) > min + abs_tol)
            # All Ports
            port.lfsource.on = False
            state[0] = state[0] and not port.lfsource.on
            state[5] = state[5] and type(port.rfreceiver) is not NoRFReceiver
        for index, value in enumerate(state):
            self.requirementsCheckBoxList[index].setChecked(value)

    def check_recommendations(self):
        super(AmplitudeRequirementsPage, self).check_recommendations()
        state = [True] * len(self.recommendationsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                pass
            else: # Measurement Port
                pass
            # All Ports
            info = port.rfztuner.info
            rel_tol, abs_tol = info["z_set"].rel_tol, info["z_set"].abs_tol
            state[0] = state[0] and np.allclose(tf.avg(port.rfztuner.z_set), 50.0, rel_tol, abs_tol)
        for index, value in enumerate(state):
            self.recommendationsCheckBoxList[index].setChecked(value)


class AmplitudeContentPage(AbstractContentPage):
    pass


class AmplitudeConclusionPage(AbstractConclusionPage):
    pass


class AmplitudeWizard(AbstractCalibrationWizard):

    _port_nums = [0, 1, 2]
    _instruments = [Instrument.RFRECEIVER,
                    Instrument.RF, Instrument.RF]

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfreceiver_ref.png")
        super(AmplitudeWizard, self).__init__(parent=parent, calkit_package=rfreceiver, calkit_icon=calkit_icon)
        self.connect_signals()
        if model is None:
            model = AmplitudeModel()
        calkit_model = model.device_model().ports[0].rfreceiver
        self.set_model(model, calkit_model)

        self.addPage(AmplitudePortPage(self))
        self.addPage(AmplitudeInstrumentPage(self))
        self.addPage(AmplitudeRequirementsPage(self))
        self.addPage(AmplitudeConclusionPage(self))

    def initialize_content_pages(self):
        super(AmplitudeWizard, self).initialize_content_pages()
        pages = list()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "RFReceiverRef"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "RFReceiverRef"]
            pages.append(AmplitudeContentPage(self, "RFReceiverRef_%d" % (port_num,), contents, optional=False))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")


class PhasePortPage(AbstractPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1 or len(self.wizard().port_indices()) == 2


class PhaseInstrumentPage(AbstractInstrumentPage):

    def __init__(self, _parent):
        super(PhaseInstrumentPage, self).__init__(_parent)
        self.add_instrument(Instrument.RFRECEIVER)


class PhaseRequirementsPage(AbstractRequirementsPage):

    def __init__(self, _parent):
        super(PhaseRequirementsPage, self).__init__(_parent)
        self.add_requirement("LF Source is OFF for all ports.")
        self.add_requirement("RF Source is connected for calibration port")
        self.add_requirement("RF Source is ON for calibration port.")
        self.add_requirement("RF Source is OFF for measurement port.")
        self.add_requirement("RF Source.a_p > min for calibration port.")
        self.add_requirement("RF Receiver is connected for all ports.")

        self.add_recommendation("RF ZTuner.z_set == 50 Ohm for all ports.")
        self.connect_signals()

    def check_requirements(self):
        super(PhaseRequirementsPage, self).check_requirements()
        state = [True]*len(self.requirementsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                state[1] = state[1] and type(port.rfsource) is not NoRFSource
                state[2] = state[2] and port.rfsource.on
                info = port.rfsource.info
                min, abs_tol = info["a_p"].min, info["a_p"].abs_tol
                state[4] = state[4] and np.all(np.abs(tf.pk(port.rfsource.a_p)) > min + abs_tol)
            else: # Measurement Port
                port.rfsource.on = False
                state[3] = state[3] and not port.rfsource.on
            # All Ports
            port.lfsource.on = False
            state[0] = state[0] and not port.lfsource.on
            state[5] = state[5] and type(port.rfreceiver) is not NoRFReceiver
        for index, value in enumerate(state):
            self.requirementsCheckBoxList[index].setChecked(value)

    def check_recommendations(self):
        super(PhaseRequirementsPage, self).check_recommendations()
        state = [True] * len(self.recommendationsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                pass
            else: # Measurement Port
                pass
            # All Ports
            info = port.rfztuner.info
            rel_tol, abs_tol = info["z_set"].rel_tol, info["z_set"].abs_tol
            state[0] = state[0] and np.allclose(tf.avg(port.rfztuner.z_set), 50.0, rel_tol, abs_tol)
        for index, value in enumerate(state):
            self.recommendationsCheckBoxList[index].setChecked(value)


class PhaseContentPage(AbstractContentPage):
    pass


class PhaseConclusionPage(AbstractConclusionPage):
    pass


class PhaseWizard(AbstractCalibrationWizard):

    _port_nums = [0, 1, 2]
    _instruments = [Instrument.RFSOURCE,
                    Instrument.RF, Instrument.RF]

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfsource_ref.png")
        super(PhaseWizard, self).__init__(parent=parent, calkit_package=rfsource, calkit_icon=calkit_icon)
        if model is None:
            model = PhaseModel()
        calkit_model = model.device_model().ports[0].rfsource
        self.set_model(model, calkit_model)

        self.addPage(PhasePortPage(self))
        self.addPage(PhaseInstrumentPage(self))
        self.addPage(PhaseRequirementsPage(self))
        self.addPage(PhaseConclusionPage(self))

    def initialize_content_pages(self):
        super(PhaseWizard, self).initialize_content_pages()
        pages = list()
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            calkit_filepath = calkit_connector_map[calkit_connector1] + "_" + calkit_connector2
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["Thru with B Wave Attenuator Connected SS_%d_%d" % (port_num1, port_num2)] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(PhaseContentPage(self, "Thru with B Wave Attenuator Connected SS_%d_%d" % (port_num1, port_num2), contents, optional=True))
            self.set_ideal(contents.index("Thru with B Wave Attenuator Connected SS_%d_%d" % (port_num1, port_num2)), os.sep.join((calkit_filepath, "thru.s2p")), pages[-1])
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["Thru with B Wave Attenuator NOT Connected SS_%d_%d" % (port_num1, port_num2)] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(PhaseContentPage(self, "Thru with B Wave Attenuator NOT Connected SS_%d_%d" % (port_num1, port_num2), contents, optional=True))
            self.set_ideal(contents.index("Thru with B Wave Attenuator NOT Connected SS_%d_%d" % (port_num1, port_num2)), os.sep.join((calkit_filepath, "thru.s2p")), pages[-1])
        port_index = self.port_indices()[0]
        port_num = port_index + 1
        port_id = "Port%d" % (port_num,)
        port_connector = self._model.port_connectors[port_index]
        calkit_connector = self._model.calkit_connectors[port_index]
        contents = [port_id, "RFSourceRef"] if port_connector == calkit_connector\
            else [port_id, "Adapter", "RFSourceRef"]
        pages.append(PhaseContentPage(self, "RFSourceRef_%d" % (port_num,), contents, optional=False))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        pass




