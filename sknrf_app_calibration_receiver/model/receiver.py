"""
    =====================================
    Receiver Calibration Models
    =====================================

    This module defines the calibration models for the receiver calibrations.

    See Also
    ----------
    sknrf.view.desktop.calibration.receiver.base.AbstractReceiverModel, sknrf.model.base.AbstractModel
"""
import logging
from collections import OrderedDict

import numpy as np
import torch as th
from scipy import interpolate

from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.settings import Settings
from sknrf.device.signal import tf, ff
from sknrf.model.calibration.base import AbstractCalibrationModel
from sknrf.enums.device import Instrument
from sknrf.model.transform.ff.base import FFTransform
from sknrf.utilities.numeric import Info
from sknrf.utilities.rf import a2t, t2a, viz2baz, gin2gl, n2t, t2n

logger = logging.getLogger(__name__)


class AmplitudeModel(AbstractCalibrationModel):
    """A calibration model for vector calibrations.

        See Also
        ----------
        AbstractReceiverModel
    """

    def __init__(self, instrument_flags=Instrument.RFRECEIVER):
        super(AmplitudeModel, self).__init__(instrument_flags=instrument_flags)
        self.measurement_type = "SS"

    def __getstate__(self, state={}):
        state = super(AmplitudeModel, self).__getstate__(state=state)
        # ### Manually save selected object PROPERTIES here ###
        state["measurement_type"] = self.measurement_type
        return state

    def __setstate__(self, state):
        super(AmplitudeModel, self).__setstate__(state)
        # ### Manually load saved object ATTRIBUTES and PROPERTIES here ###
        self.measurement_type = state["measurement_type"]

    def __info__(self):
        """ Initializes the display information of a device and stores information in self.info.
        """
        super(AmplitudeModel, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["measurement_type"] = Info("measurement type", read=True, write=True, check=False)

    def calculate(self):
        ports = self.device_model().ports
        port_index = self.port_indices[-1]
        try:
            last_transform = self.device_model().transforms[-1]
            if not isinstance(last_transform, FFTransform):
                raise TypeError
        except IndexError:
            raise IndexError("Could not find pre-existing Transform")
        except TypeError:
            raise TypeError("Could not find pre-existing Frequency Transform")

        abcd_index = last_transform.ports.index(port_index)
        abcd = last_transform._abcd[:, 2*abcd_index:2*abcd_index+2, 2*abcd_index:2*abcd_index+2]
        t = a2t(abcd)[0]
        alpha = t[:, 1, 1].reshape(-1, 1, 1)
        t = t/alpha
        beta = t[:, 1, 0].reshape(-1, 1, 1)

        page_name = "RFReceiverRef"
        port_nums = "_%d" % (port_index,)
        num_fund = Settings().t_points
        freq = ff.freq()

        dataset = self.datagroup_model()["AmplitudeModel"].dataset(page_name + port_nums)
        v = n2t(dataset.__getattr__("v_%d" % (port_index,))[...])
        i = n2t(dataset.__getattr__("i_%d" % (port_index,))[...])
        z = n2t(dataset.__getattr__("z_%d" % (port_index,))[...])
        b = n2t(dataset.__getattr__("b_%d" % (port_index,))[...])
        a = n2t(dataset.__getattr__("a_%d" % (port_index,))[...])
        g = n2t(dataset.__getattr__("g_%d" % (port_index,))[...])
        b[..., 0:1], a[..., 0:1], g[..., 0:1] = viz2baz(v, i, z)
        b, a, g = tf.ff(b), tf.ff(a), tf.ff(g)

        v0 = n2t(dataset.__getattr__("v_%d" % (port_index,))[...])
        i0 = n2t(dataset.__getattr__("i_%d" % (port_index,))[...])
        z0 = n2t(dataset.__getattr__("z_%d" % (port_index,))[...])
        b0 = n2t(dataset.__getattr__("b_%d" % (port_index,))[...])
        a0 = n2t(dataset.__getattr__("a_%d" % (port_index,))[...])
        g0 = n2t(dataset.__getattr__("g_%d" % (port_index,))[...])
        b0[..., 0:1], a0[..., 0:1], g0[..., 0:1] = viz2baz(v0, i0, z0)
        b0, a0, g0 = tf.ff(b0), tf.ff(a0), tf.ff(g0)

        s_shape = th.prod(th.as_tensor(dataset.shape[-2:])), dataset.shape[-5], dataset.shape[-5]
        B = th.zeros(s_shape, dtype=si_dtype_map[SI.B])
        A = th.zeros(s_shape, dtype=si_dtype_map[SI.A])
        B0 = th.zeros(s_shape, dtype=si_dtype_map[SI.B])
        time_index = slice(1, 2, 1) if num_fund == 2 else slice(None)
        for harm_index in range(Settings().f_points-1):
            for fund_index in range(Settings().t_points):
                B[num_fund*harm_index:num_fund*(harm_index+1), 0, 0] = b[..., harm_index, fund_index, time_index, harm_index]
                A[num_fund*harm_index:num_fund*(harm_index+1), 0, 0] = a[..., harm_index, fund_index, time_index, harm_index]
                B0[num_fund*harm_index:num_fund*(harm_index+1), 0, 0] = b0[..., harm_index, fund_index, time_index, harm_index]
        if page_name + port_nums in self.ideal_ntwks:
            T_a = th.as_tensor(self.ideal_ntwks[page_name + port_nums].s[:, 1, 0], dtype=b0.dtype)
            f = th.as_tensor(self.ideal_ntwks[page_name + port_nums].f, dtype=b0.dtype)
            freq_interp = interpolate.interp1d(f, T_a, kind="linear", assume_sorted=True)
            T_a = freq_interp(freq).reshape(-1, 1, 1)
        else:
            T_a = th.as_tensor(1, dtype=b0.dtype).reshape(-1, 1, 1)
        T_a = T_a + th.zeros(alpha.shape, dtype=T_a.dtype)

        K_t = th.abs(B0/(T_a*(A + beta*B)))
        self.calibration = t2a(K_t*t)[0][:, 0, 0]/abcd[:, 0, 0].abs()

    def apply_cal(self):
        name = "abs cal"
        transforms = self.device_model().transforms
        num_ports = len(self.device_model().ports)-1
        port_list = tuple(range(1, num_ports+1))
        abcd = self.calibration.reshape(-1, 1, 1)*th.eye(2*num_ports, dtype=self.calibration.dtype)
        transform = FFTransform(name, port_list, instrument_flags=self.instrument_flags,
                                data=abcd)
        transforms.append(name, transform)


class PhaseModel(AbstractCalibrationModel):
    """A calibration model for vector calibrations.

        See Also
        ----------
        AbstractReceiverModel
    """

    def __init__(self, instrument_flags=Instrument.RFRECEIVER):
        super(PhaseModel, self).__init__(instrument_flags=instrument_flags)
        self.measurement_type = "SS"
        self._fs_transforms = OrderedDict()

    def __getstate__(self, state={}):
        state = super(PhaseModel, self).__getstate__(state=state)
        # ### Manually save selected object PROPERTIES here ###
        state["measurement_type"] = self.measurement_type
        state["_fs_transforms"] = self._fs_transforms
        return state

    def __setstate__(self, state):
        super(PhaseModel, self).__setstate__(state)
        # ### Manually load saved object ATTRIBUTES and PROPERTIES here ###
        self.measurement_type = state["measurement_type"]
        self._fs_transforms = state["_fs_transforms"]

    def __info__(self):
        """ Initializes the display information of a device and stores information in self.info.
        """
        super(PhaseModel, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["measurement_type"] = Info("measurement type", read=False, write=False, check=False)

    def measure(self, page_name, port_nums, calkit_standard):
        if page_name == "RFSourceRef":
            # Todo: Rmove Adapter
            measure, func = super(PhaseModel, self).measure(page_name, port_nums, calkit_standard)
            measure.ss_ports[0] = 0
            return measure, func
        else:
            return super(PhaseModel, self).measure(page_name, port_nums, calkit_standard)

    def set_measurement(self, page_name, port_nums, calkit_standard):
        super(PhaseModel, self).set_measurement(page_name, port_nums, calkit_standard)
        if page_name == "RFSourceRef":
            pass
            # Todo: Add Adapter back

    def calculate(self):
        port_index = self.port_indices[0]

        try:
            last_transform = self.device_model().transforms[-1]
            if not isinstance(last_transform, FFTransform):
                raise TypeError
        except IndexError:
            raise IndexError("Could not find pre-existing Transform")
        except TypeError:
            raise TypeError("Could not find pre-existing Frequency Transform")

        abcd_index = last_transform.ports.index(port_index)
        abcd = last_transform._abcd[:, 2*abcd_index:2*abcd_index+2, 2*abcd_index:2*abcd_index+2]
        t = a2t(abcd)[0]
        alpha = t[:, 1, 1].reshape(-1, 1, 1)
        t = t/alpha
        beta = t[:, 1, 0].reshape(-1, 1, 1)
        gamma = t[:, 1, 1].reshape(-1, 1, 1)
        delta = t[:, 0, 0].reshape(-1, 1, 1)

        page_name = "RFSourceRef"
        port_nums = "_%d" % (port_index, )
        num_fund = Settings().t_points
        if len(self.port_indices) == 2:
            dataset_with = "Thru_with_B_Wave_Attenuator_Connected_SS_%d_%d" % (self.port_indices[0], self.port_indices[1])
            dataset_without = "Thru_with_B_Wave_Attenuator_NOT_Connected_SS_%d_%d" % (self.port_indices[0], self.port_indices[1])
        else:
            dataset_with = ""
            dataset_without = ""
        dg = self.datagroup_model()["PhaseModel"]
        if dg.has_dataset(dataset_with) and dg.has_dataset(dataset_without):
            s_with = th.as_tensor(dg.dataset(dataset_with).s, dtype=si_dtype_map[SI.B]).reshape(-1, 2, 2)
            s_without = th.as_tensor(dg.dataset(dataset_without).s, dtype=si_dtype_map[SI.B]).reshape(-1, 2, 2)
            s_atten = s_without
            s_atten[:, 0, 1] = s_with[:, 0, 1]/s_without[:, 0, 1]
            s_atten = s_atten
        else:
            s_atten = th.as_tensor([[0, 1],[1, 0]], dtype=si_dtype_map[SI.B]).reshape(-1, 2, 2)
        freq = ff.freq()

        dataset = dg.dataset(page_name + port_nums)
        v = n2t(dataset.__getattr__("v_%d" % (port_index,))[...])
        i = n2t(dataset.__getattr__("i_%d" % (port_index,))[...])
        z = n2t(dataset.__getattr__("z_%d" % (port_index,))[...])
        b = n2t(dataset.__getattr__("b_%d" % (port_index,))[...])
        a = n2t(dataset.__getattr__("a_%d" % (port_index,))[...])
        g = n2t(dataset.__getattr__("g_%d" % (port_index,))[...])
        b[..., 0:1], a[..., 0:1], g[..., 0:1] = viz2baz(v, i, z)
        b, a, g = tf.ff(b), tf.ff(a), tf.ff(g)

        v0 = n2t(dataset.__getattr__("v_%d" % (port_index,))[...])
        i0 = n2t(dataset.__getattr__("i_%d" % (port_index,))[...])
        z0 = n2t(dataset.__getattr__("z_%d" % (port_index,))[...])
        b0 = n2t(dataset.__getattr__("b_%d" % (port_index,))[...])
        a0 = n2t(dataset.__getattr__("a_%d" % (port_index,))[...])
        g0 = n2t(dataset.__getattr__("g_%d" % (port_index,))[...])
        b0[..., 0:1], a0[..., 0:1], g0[..., 0:1] = viz2baz(v0, i0, z0)
        b0, a0, g0 = tf.ff(b0), tf.ff(a0), tf.ff(g0)

        s_shape = th.prod(th.as_tensor(dataset.shape[-2:])), dataset.shape[-5], dataset.shape[-5]
        B = th.zeros(s_shape, dtype=si_dtype_map[SI.B])
        A = th.zeros(s_shape, dtype=si_dtype_map[SI.A])
        A0 = th.zeros(s_shape, dtype=si_dtype_map[SI.A])
        time_index = slice(1, 2, 1) if num_fund == 2 else slice(None)
        for harm_index in range(Settings().f_points - 1):
            for fund_index in range(Settings().t_points):
                B[num_fund * harm_index:num_fund * (harm_index + 1), 0, 0] = b[..., harm_index, fund_index, time_index, harm_index]
                A[num_fund * harm_index:num_fund * (harm_index + 1), 0, 0] = a[..., harm_index, fund_index, time_index, harm_index]
                A0[num_fund * harm_index:num_fund * (harm_index + 1), 0, 0] = a0[..., harm_index, fund_index, time_index, harm_index]
        A0 *= s_atten[:, 0:1, 1:2]

        if page_name + "SS" + port_nums in self.ideal_ntwks:
            G_p = th.as_tensor(self.measured_ntwks[page_name + "SS" + port_nums].s[:, 0, 0], dtype=a0.dtype)
            G_p = gin2gl(G_p, s_atten)[0]
            f = th.as_tensor(self.ideal_ntwks[page_name + port_nums].f, dtype=a0.dtype)
            freq_interp = interpolate.interp1d(f, G_p, kind="linear", assume_sorted=True)
            G_p = freq_interp(freq).reshape(-1, 1, 1)
        else:
            G_p = th.as_tensor(0, dtype=a0.dtype).reshape(-1, 1, 1)
        G_p = G_p + th.zeros(alpha.shape, dtype=G_p.dtype)

        K_t = np.angle(t2n(A0/((gamma - G_p)*A + (delta - G_p*beta)*B)))

        old_phase = np.angle(abcd[:, 0, 0])
        new_phase = np.angle(t2a(n2t(np.exp(1j*K_t)*t2n(t)))[0][:, 0, 0])
        self.calibration = n2t(np.exp(1j*(new_phase-old_phase)))

    def apply_cal(self):
        name = "phase cal"
        transforms = self.device_model().transforms
        num_ports = len(self.device_model().ports)-1
        port_list = tuple(range(1, num_ports+1))
        abcd = self.calibration.reshape(-1, 1, 1)*th.eye(2*num_ports, dtype=self.calibration.dtype)
        transform = FFTransform(name, port_list, instrument_flags=self.instrument_flags,
                                data=abcd)
        transforms.append(name, transform)
