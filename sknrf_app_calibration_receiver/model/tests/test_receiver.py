import os
import unittest

from sknrf.settings import Settings
from sknrf.model.base import AbstractModel
from sknrf_app_calibration_receiver.model.receiver import AmplitudeModel, PhaseModel
from sknrf.model.calibration.tests.test_calibration import CalibrationMethodTests
from sknrf_transform_ff_system.model.system import CascadeTransform
from sknrf.enums.device import Instrument


class TestAmplitudeCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = AmplitudeModel
    cal_port_indices = [1]
    cal_measurements = [["RFReceiverRef", "_1", "RFReceiverRef"],]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "AmplitudeModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def setUp(self):
        super(TestAmplitudeCalibrationMethod, self).setUp()
        AbstractModel.device_model().transforms.append("Vector", CascadeTransform(instrument_flags=Instrument.RF))

    def test_measurement(self):
        super(TestAmplitudeCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestAmplitudeCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestAmplitudeCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        pass

    def test_calculate_rf(self):
        super(TestAmplitudeCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestAmplitudeCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestAmplitudeCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestAmplitudeCalibrationMethod, self).test_apply()


class TestPhaseCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = PhaseModel
    cal_port_indices = [1]
    cal_measurements = [["RFSourceRef", "_1", "RFSourceRef"],]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "PhaseModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def setUp(self):
        super(TestPhaseCalibrationMethod, self).setUp()
        AbstractModel.device_model().transforms.append("Vector", CascadeTransform(instrument_flags=Instrument.RF))

    def test_measurement(self):
        super(TestPhaseCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestPhaseCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestPhaseCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        pass

    def test_calculate_rf(self):
        super(TestPhaseCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestPhaseCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestPhaseCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestPhaseCalibrationMethod, self).test_apply()


def vector_calibration_test_suite():
    test_suite = unittest.TestSuite()

    test_suite.addTest(unittest.makeSuite(TestAmplitudeCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestPhaseCalibrationMethod))

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(vector_calibration_test_suite())

